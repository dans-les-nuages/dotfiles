function! myspacevim#before() abort
  set mouse=
  :map nnn :setlocal nonumber norelativenumber<cr>
endfunction

function! myspacevim#after() abort
  :imap << <Esc>:wq<cr>
  :map << <Esc>:wq<cr>
  :nmap << :wq<cr>
  :imap ;; <Esc>:w<cr>
  :map ;; <Esc>:w<cr>
  :nmap ;; :w<cr>
endfunction
